<?php

namespace bizr\sociaLink\Facades;

use Illuminate\Support\Facades\Facade;

class sociaLink extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'socialink';
    }
}
