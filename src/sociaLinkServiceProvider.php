<?php

namespace bizr\sociaLink;

use Illuminate\Support\ServiceProvider;

class sociaLinkServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'bizr');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'bizr');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {

            // Publishing the configuration file.
            $this->publishes([
                __DIR__ . '/../config/socialink.php' => config_path('socialink.php'),
            ], 'socialink.config');

            // Publishing the views.
            /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/bizr'),
            ], 'socialink.views');*/

            // Publishing assets.
            /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/bizr'),
            ], 'socialink.views');*/

            // Publishing the translation files.
            /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/bizr'),
            ], 'socialink.views');*/

            // Registering package commands.
            // $this->commands([]);
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/socialink.php', 'socialink');

        // Register the service the package provides.
        $this->app->singleton('socialink', function ($app) {
            return new sociaLink;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['socialink'];
    }
}
