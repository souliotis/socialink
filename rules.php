<?php

// Available placeholders: bizr, sociaLink, bizr, socialink
return [
    'src/MyPackage.php' => 'src/sociaLink.php',
    'config/mypackage.php' => 'config/socialink.php',
    'src/Facades/MyPackage.php' => 'src/Facades/sociaLink.php',
    'src/MyPackageServiceProvider.php' => 'src/sociaLinkServiceProvider.php',
];