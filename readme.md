# sociaLink

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

This is where your description should go. Take a look at [contributing.md](contributing.md) to see a to do list.

## Installation

Via Composer

``` bash
$ composer require bizr/socialink
```

## Usage

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email info@bizr.gr instead of using the issue tracker.

## Credits

- [bizr][link-author]
- [All Contributors][link-contributors]

## License

license. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/bizr/socialink.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/bizr/socialink.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/bizr/socialink/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/bizr/socialink
[link-downloads]: https://packagist.org/packages/bizr/socialink
[link-travis]: https://travis-ci.org/bizr/socialink
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/bizr
[link-contributors]: ../../contributors]